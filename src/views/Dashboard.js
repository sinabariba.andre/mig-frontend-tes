import React from "react";
import Activity from "../components/Activity";
import BankAccount from "../components/BankAccount";
import CompanyProfile from "../components/CompanyProfile";
import LocationContainer from "../components/LocationContainer";
import RelationContainer from "../components/RelationContainer";

const Dashboard = () => {
  return (
    <div className="my-4 mx-6 min-h-[100vh] flex gap-6">
      <section id="company-profile" className="min-w-[290px]">
        <CompanyProfile />
      </section>
      <section id="location" className="w-full h0">
        <LocationContainer />
        <div className="grid grid-cols-2 bg-red my-5 gap-8">
          <div className="flex flex-col gap-6">
            <BankAccount />
            <RelationContainer />
          </div>
          <Activity />
        </div>
      </section>
    </div>
  );
};

export default Dashboard;
