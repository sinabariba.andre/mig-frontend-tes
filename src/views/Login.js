import React, { useState } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import { FaUserSecret } from "react-icons/fa";
import { RiKey2Fill } from "react-icons/ri";
import axios from "axios";
import { useDispatch } from "react-redux";

const Login = () => {
  const dispatch = useDispatch();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = useNavigate();

  const handleLogin = async (e) => {
    e.preventDefault();
    try {
      await axios
        .post("https://mitramas-test.herokuapp.com/auth/login", {
          email,
          password,
        })
        .then((response) => {
          dispatch({ type: "SET_USER", currentUser: response.data });
        });
      history("/dashboard");
    } catch (error) {}
    <Navigate to="/dashboard" />;
  };
  return (
    <div className="bg-light min-h-[100vh] flex flex-row justify-center p-5 bg-bground">
      <div className="w-[90%] md:w-[75%]  rounded-lg p-4 flex flex-col items-center justify-center gap-4">
        <div className="min-w-[50%]  justify-content-center bg-white py-16 px-8 shadow-lg rounded-md">
          <form onSubmit={handleLogin} method="POST">
            <h1 className="text-center">Login</h1>
            <p className="text-center pb-10">Sign In to your account</p>
            <div className="w-full flex items-center justify-between mb-3  gap-3">
              <FaUserSecret className="text-3xl" />
              <input
                placeholder="Email"
                type="text"
                className="w-full border-none rounded-md placeholder:text-sm placeholder:font-semibold shadow-md"
                name="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                autoComplete="email"
                required
              />
            </div>
            <div className="w-full flex items-center justify-between mb-3  gap-3">
              <RiKey2Fill className="text-3xl" />
              <input
                type="password"
                name="password"
                value={password}
                className="w-full border-none rounded-md placeholder:text-sm placeholder:font-semibold shadow-md"
                onChange={(e) => setPassword(e.target.value)}
                placeholder="Password"
                autoComplete="current-password"
                required
              />
            </div>

            <div className="flex justify-center">
              <button
                type="submit"
                className="px-7 text-white text-center py-1 mt-5 bg-emerald-500 hover:bg-emerald-600 rounded-md"
              >
                Login
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Login;
