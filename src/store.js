/* eslint-disable import/no-anonymous-default-export */
import { createStore } from "redux";

import { persistReducer, persistStore } from "redux-persist";
import storage from "redux-persist/lib/storage";

import { composeWithDevTools } from "redux-devtools-extension";

const initialState = {
  sidebarShow: true,
  authed: false,
  currentUser: null,
  customers: {},
};

const persistConfig = {
  key: "root",
  storage,
  whitelist: ["authed", "currentUser", "customers"],
};

const changeState = (state = initialState, { type, ...rest }) => {
  switch (type) {
    case "set":
      return { ...state, ...rest };
    case "SET_USER":
      return { ...state, currentUser: { ...rest }, authed: true };
    case "GET_USER":
      return { ...state, currentUser: { ...rest }, authed: true };
    case "GET_CUSTOMERS":
      return { ...state, customers: { ...rest } };
    case "logout":
      return initialState;
    default:
      return state;
  }
};

export const store = createStore(
  persistReducer(persistConfig, changeState),
  composeWithDevTools()
);
export const persistor = persistStore(store);
export default { store, persistor };
