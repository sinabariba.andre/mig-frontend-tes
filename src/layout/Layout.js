import React from "react";
import { AppContent, AppHeader, AppSidebar } from "../components/";

const Layout = () => {
  return (
    <div className="flex">
      <AppSidebar />
      <div className="wrapper w-screen flex flex-col min-h-[100vh] bg-bground">
        <AppHeader />
        <div className="body flex-grow-[1] px-3 bg-bground">
          <AppContent />
        </div>
      </div>
    </div>
  );
};

export default Layout;
