import React from "react";
import { BiBuildingHouse } from "react-icons/bi";
import { IoBuildOutline } from "react-icons/io5";
import { TbBuildingWarehouse } from "react-icons/tb";

const LocationList = () => {
  return (
    <div>
      <div className="flex justify-between">
        <p className="text-l font-semibold">Lokasi</p>
        <button className="text-xs text-emerald-700">Lihat semua</button>
      </div>
      <div className="w-full flex flex-wrap md:grid md:grid-cols-3 auto-rows-max py-5 gap-3">
        <div className="bg-locList  w-full  flex items-center justify-between  p-5 rounded-md ">
          <BiBuildingHouse className="text-white text-4xl" />
          <div className="flex flex-col items-end justify-end">
            <p className="text-white text-3xl">20</p>{" "}
            <p className="text-gray-light text-xs">Induk</p>
          </div>
        </div>
        <div className="bg-locList  w-full flex items-center justify-between  p-5 rounded-md ">
          <IoBuildOutline className="text-white text-4xl -rotate-90" />
          <div className="flex flex-col items-end justify-end">
            <p className="text-white text-3xl">3</p>
            <p className="text-gray-light text-xs">Sub Lokasi Level 1 </p>
          </div>
        </div>
        <div className="bg-locList  w-full flex items-center justify-between  p-5 rounded-md ">
          <TbBuildingWarehouse className="text-white text-4xl" />
          <div className="flex flex-col items-end justify-end">
            <p className="text-white text-3xl">1</p>{" "}
            <p className="text-gray-light text-xs">Sub Lokasi Level 1 </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LocationList;
