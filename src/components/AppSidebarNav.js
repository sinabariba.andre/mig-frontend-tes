import React from "react";
import { BiBuilding, BiHome } from "react-icons/bi";
import {
  BsBoundingBoxCircles,
  BsClipboardCheck,
  BsTriangleFill,
} from "react-icons/bs";
import { TbBox, TbBuildingSkyscraper, TbTicket, TbUsers } from "react-icons/tb";
import { FaSearchDollar } from "react-icons/fa";
import { FiLayers } from "react-icons/fi";
import { CgBox } from "react-icons/cg";
import { Link } from "react-router-dom";

const AppSidebarNav = () => {
  return (
    <div className="w-full  flex  flex-col gap-6 ">
      <Link
        to="/"
        className="flex relative items-center justify-center flex-col"
      >
        <BsTriangleFill className=" text-3xl text-emerald-600" />
        <BsTriangleFill className="relative text-3xl z-10 -top-6 left-2  text-cyan-600 opacity-70" />
      </Link>
      <div className="flex flex-col gap-5 my-5">
        <Link to="/" className="flex items-center justify-center flex-col">
          <BiHome className="text-2xl text-gray-dark" />
        </Link>
        <Link to="#" className="flex items-center justify-center flex-col">
          <TbTicket className="text-2xl text-gray-dark" />
        </Link>
        <Link to="#" className="flex items-center justify-center flex-col">
          <BsClipboardCheck className="text-xl text-gray-dark" />
        </Link>
        <Link to="#" className="flex items-center justify-center flex-col">
          <CgBox className="text-2xl text-gray-dark" />
        </Link>
        <Link
          to="#"
          className="active bg-gray-light p-5 flex items-center justify-center flex-col"
        >
          <TbBuildingSkyscraper className="text-2xl text-gray-dark" />
        </Link>
      </div>

      <div className="flex flex-col gap-5 my-5">
        <Link to="/" className="flex items-center justify-center flex-col">
          <TbUsers className="text-2xl text-gray-dark" />
        </Link>
        <Link to="#" className="flex items-center justify-center flex-col">
          <FiLayers className="text-2xl text-gray-dark" />
        </Link>
        <Link to="#" className="flex items-center justify-center flex-col">
          <TbBox className="text-3xl text-gray-dark" />
        </Link>
        <Link to="#" className="flex items-center justify-center flex-col">
          <BsBoundingBoxCircles className="text-xl text-gray-dark" />
        </Link>
        <Link to="#" className="flex items-center justify-center flex-col">
          <FaSearchDollar className="text-2xl text-gray-dark" />
        </Link>
      </div>
      <div className="flex flex-col gap-5 my-5">
        <Link to="/" className="flex items-center justify-center flex-col">
          <BiBuilding className="text-2xl text-gray-dark" />
        </Link>
      </div>
    </div>
  );
};

export default AppSidebarNav;
