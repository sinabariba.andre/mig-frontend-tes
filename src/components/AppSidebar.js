import React from "react";
import AppSidebarNav from "./AppSidebarNav";

const AppSidebar = () => {
  return (
    <div className="w-16 h-screen  flex justify-center py-5 ">
      <AppSidebarNav />
    </div>
  );
};

export default AppSidebar;
