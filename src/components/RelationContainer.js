import React from "react";
import { BsShare } from "react-icons/bs";

const RelationContainer = () => {
  return (
    <div className="w-full bg-white p-5 shadow-lg rounded-md  flex flex-col gap-5 pb-6">
      <div className="flex justify-between">
        <p className="text-lg font-semibold">Relasi</p>
        <button className="text-sm text-emerald-700 cursor-pointer">
          Lihat Semua
        </button>
      </div>
      <div className="flex items-center gap-5 mt-5">
        <BsShare className="text-2xl" />
        <div>
          <p className="text-xl font-semibold">20</p>
          <p className="text-xs text-gray">Memiliki</p>
        </div>
      </div>
      <div className="flex items-center gap-5">
        <BsShare className="text-2xl" />
        <div>
          <p className="text-xl font-semibold">108</p>
          <p className="text-xs text-gray">Menggunakan</p>
        </div>
      </div>
      <div className="flex items-center gap-5">
        <BsShare className="text-2xl" />
        <div>
          <p className="text-xl font-semibold">67</p>
          <p className="text-xs text-gray">Meminjam</p>
        </div>
      </div>
    </div>
  );
};

export default RelationContainer;
