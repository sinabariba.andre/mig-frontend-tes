import React from "react";

const Activity = () => {
  const activities = [
    {
      id: 1,
      activity:
        "Yusron baru saja menambahkan lokasi baru Kantor Cabang Jagakarsa",
      time: "Hari ini 06:00",
    },
    {
      id: 2,
      activity:
        "Bintang baru saja menghapus sublokasi KCP Tebet dari induk Kantor Cabang Tebet",
      time: "Kemarin 17:32",
    },
    {
      id: 3,
      activity:
        "Yusron melakukan perubahan profile pada induk Kantor Cabang Bekasi",
      time: "Kemarin 17:32",
    },
  ];
  return (
    <div className="w-full bg-white p-5 shadow-md rounded-md">
      <p className="text-lg font-semibold">Aktivitas</p>

      <div className="flex flex-col py-6 gap-10">
        {activities &&
          activities.map((data) => (
            <div key={data.id} className="w-full">
              <p className="text-md">{data.activity}</p>
              <p className="text-xs text-gray">{data.time}</p>
            </div>
          ))}
      </div>
    </div>
  );
};

export default Activity;
