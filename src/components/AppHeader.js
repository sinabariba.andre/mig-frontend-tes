import React, { useState } from "react";
import { BiBell, BiSearch } from "react-icons/bi";
import { Link } from "react-router-dom";
import avatar from "../assets/avatar.jpg";
import { motion } from "framer-motion";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

const AppHeader = () => {
  const [isOpen, setIsOpen] = useState(false);
  const dispatch = useDispatch();
  const history = useNavigate();
  const loggedOut = () => {
    dispatch({ type: "logout" });
    history("/login");
  };
  return (
    <header className="w-full bg-transparent flex justify-between items-center p-4 shadow-md">
      {/* desktop & tablet */}
      <div className="hidden  md:flex w-full items-center justify-between px-4">
        <nav className="flex" aria-label="Breadcrumb">
          <ol className="inline-flex items-center space-x-1 md:space-x-3">
            <li>
              <div className="flex items-center">
                <Link
                  to="#"
                  className="ml-1 text-sm font-medium text-gray hover:text-gray-900 md:ml-2 dark:text-gray-400 dark:hover:text-white"
                >
                  Perusahaan
                </Link>
              </div>
            </li>
            <li aria-current="page">
              <div className="flex items-center">
                <svg
                  className="w-6 h-6 text-gray-400"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                    clipRule="evenodd"
                  ></path>
                </svg>
                <span className="ml-1 text-sm font-medium text-gray-500 md:ml-2 dark:text-gray-400">
                  Mitramas Infosys Global
                </span>
              </div>
            </li>
          </ol>
        </nav>
        <div className="flex items-center  gap-6">
          <div className="flex items-center justify-center gap-3">
            <motion.div
              whileHover={{ scale: 1.2 }}
              whileTap={{ scale: 0.75 }}
              className="cursor-pointer"
            >
              <BiSearch className="text-2xl " />
            </motion.div>
            <motion.div
              whileHover={{ scale: 1.2 }}
              whileTap={{ scale: 0.75 }}
              className="cursor-pointer"
            >
              <BiBell className="text-2xl " />
            </motion.div>
          </div>
          <div className="flex items-center gap-3 relative cursor-pointer px-2">
            <motion.img
              whileHover={{ scale: 1.2 }}
              whileTap={{ scale: 0.75 }}
              src={avatar}
              alt="profil"
              className="w-8 rounded-full shadow-md"
              onClick={() => setIsOpen(!isOpen)}
            />
            {isOpen && (
              <button
                className="text-xs absolute z-10 -bottom-6"
                onClick={loggedOut}
              >
                Logout
              </button>
            )}
            <p className="text-sm font-semibold">John Doe</p>
          </div>
        </div>
      </div>

      {/* mobile */}
      <div className="flex items-center justify-between md:hidden w-full h-full py-2 px-6">
        <div className="w-full  flex items-center justify-between">
          <div className="flex items-center justify-center gap-3">
            <motion.div
              whileHover={{ scale: 1.2 }}
              whileTap={{ scale: 0.75 }}
              className="cursor-pointer"
            >
              <BiSearch className="text-2xl " />
            </motion.div>
            <motion.div
              whileHover={{ scale: 1.2 }}
              whileTap={{ scale: 0.75 }}
              className="cursor-pointer"
            >
              <BiBell className="text-2xl " />
            </motion.div>
          </div>
          <div className="flex items-center relative gap-3 cursor-pointer">
            <motion.img
              whileHover={{ scale: 1.2 }}
              whileTap={{ scale: 0.75 }}
              src={avatar}
              alt="profil"
              className="w-8 rounded-full shadow-md"
              onClick={() => setIsOpen(!isOpen)}
            />
            {isOpen && (
              <button
                className="text-xs absolute z-10 -bottom-5"
                onClick={loggedOut}
              >
                Logout
              </button>
            )}
            <p className="text-sm ">John Doe</p>
          </div>
        </div>
      </div>
    </header>
  );
};

export default AppHeader;
