import React from "react";

import { BiPencil, BiTrash } from "react-icons/bi";

const BankAccount = () => {
  return (
    <div className="w-full bg-white p-5 drop-shadow-md rounded-md">
      <div className="flex flex-col">
        <div className="flex justify-between items-center">
          <p className="text-lg font-semibold">Akun Bank</p>
          <button className="w-auto bg-locList py-1 px-5 text-sm rounded-md text-white">
            + Tambah Akun Bank
          </button>
        </div>
        {/* account */}
        <div className="w-full flex  py-5 gap-5">
          <div className="w-36 h-24 relative bg-gradient-to-br from-yellow to-lime-600 rounded-md">
            <p className="absolute bottom-3 font-semibold text-lg italic text-white right-3">
              VISA
            </p>
          </div>
          <div className="w-full flex flex-col">
            <div className="flex items-center justify-between">
              <p className="text-sm flex w-full font-semibold">
                Bank KB Bukopin
              </p>
              <div className="flex gap-2">
                <BiPencil className="text-emerald-700 cursor-pointer" />
                <BiTrash className="text-red-600 cursor-pointer" />
              </div>
            </div>
            <div className="mt-8">
              <p className="text-gray-light text-xs">
                **** 0876 - Yusron Taufiq
              </p>
              <p className="text-gray-light">IDR</p>
            </div>
          </div>
        </div>

        <div className="w-full flex py-5 gap-5">
          <div className="w-36 h-24 relative bg-gradient-to-r from-blue to-cyan-300 rounded-md">
            <p className="absolute bottom-3 font-semibold text-lg italic text-white right-3">
              VISA
            </p>
          </div>
          <div className="w-full flex flex-col">
            <div className="flex items-center justify-between">
              <p className="font-semibold">Citibank, NA</p>
              <div className="flex gap-2">
                <BiPencil className="text-emerald-700 cursor-pointer" />
                <BiTrash className="text-red-600 cursor-pointer" />
              </div>
            </div>
            <div className="mt-8">
              <p className="text-gray-light text-xs">**** 5525 - Si Tampan</p>
              <p className="text-gray-light">USD</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BankAccount;
