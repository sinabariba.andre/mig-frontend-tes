import React from "react";
import LocationList from "./LocationList";

const LocationContainer = () => {
  return (
    <div className="w-full bg-white p-5 drop-shadow-md rounded-md">
      <LocationList />
    </div>
  );
};

export default LocationContainer;
