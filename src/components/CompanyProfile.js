/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import company from "../assets/company.jpeg";
import logo from "../assets/logo.png";
import { BiPencil } from "react-icons/bi";
import { BsGlobe } from "react-icons/bs";
import { IoCallOutline } from "react-icons/io5";
import { MdMailOutline } from "react-icons/md";
const CompanyProfile = () => {
  return (
    <div className="h-auto bg-white rounded-md drop-shadow-md pb-16">
      {/* profile */}
      <div className="w-full flex flex-col items-center justify-center ">
        <div className="relative w-full h-[100px]">
          <img
            src={company}
            alt=""
            className="abosolute w-full h-full rounded-t-md"
          />
        </div>
        <img
          alt=""
          src={logo}
          className="w-32 relative bg-white rounded-full -top-10 p-3 border border-locList"
        />
        <div className="flex items-center flex-col">
          <h2 className="text-lg semibold">Mitramas Infosys Global</h2>
          <p className="text-sm text-gray">Layanan IT</p>
          <button className="text-sm text-locList flex items-center my-5 gap-2">
            <BiPencil className="text-sm text-emerald-700" /> Sunting profile
          </button>
        </div>
      </div>

      {/* company info  */}
      <div className="w-full px-6">
        <div className="w-full py-2 flex flex-col gap-1">
          <p className="text-xs text-gray tracking-wide">Status Perusahaan</p>
          <p className="text-sm tracking-wider">Aktif</p>
        </div>
        <div className="w-full py-2 flex flex-col gap-1">
          <p className="text-xs text-gray tracking-wide">Singkatan</p>
          <p className="text-sm tracking-wider">MIG</p>
        </div>
        <div className="w-full py-2 flex flex-col gap-1">
          <p className="text-xs text-gray tracking-wide">Alamat Perusahaan</p>
          <p className="text-sm tracking-wider">
            Jl. Tebet Raya No.42, Jakarta Selatan
          </p>
        </div>
        <div className="w-full py-2 flex flex-col gap-1">
          <p className="text-xs text-gray tracking-wide">
            Penanggung Jawab (PIC)
          </p>
          <p className="text-sm tracking-wider">John Doe</p>
        </div>
        <div className="w-full py-2 flex flex-col gap-1">
          <p className="text-xs text-gray tracking-wide">Tanggal PKP</p>
          <p className="text-sm tracking-wider">03 Maret 2021</p>
        </div>
        <div className="w-full py-2 flex flex-col gap-1">
          <p className="text-xs text-gray tracking-wide">Email</p>
          <p className="flex items-center gap-2 text-sm tracking-wider text-emerald-700 underline">
            <MdMailOutline className="text-emerald-700" /> mig@mitrasolusi.group
          </p>
        </div>
        <div className="w-full py-2 flex flex-col gap-1">
          <p className="text-xs text-gray tracking-wide">No. Telp</p>
          <p className="text-sm flex items-center gap-2 text-emerald-700 tracking-wider">
            <IoCallOutline className="text-emerald-700" /> 021-5678-1234
          </p>
        </div>
        <div className="w-full py-2 flex flex-col gap-1">
          <p className="text-xs text-gray tracking-wide">Situs Web</p>
          <a
            href=""
            className="flex items-center text-sm text-emerald-700 underline gap-2 tracking-wider"
          >
            <BsGlobe className="text-emerald-700" /> mitramas.com
          </a>
        </div>
      </div>
    </div>
  );
};

export default CompanyProfile;
