import React from "react";
import { Outlet, Navigate } from "react-router-dom";
import { useSelector } from "react-redux";

const PrivateOutlet = () => {
  const authed = useSelector((state) => state.authed);
  // const authed = true;
  return authed ? <Outlet /> : <Navigate to="/login" />;
};

export default PrivateOutlet;
