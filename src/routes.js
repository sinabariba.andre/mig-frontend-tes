import React from "react";
import Login from "./views/Login";

const Dashboard = React.lazy(() => import("./views/Dashboard"));

const routes = [
  { path: "/", exact: true, name: "Home" },
  { path: "/dashboard", name: "Dashboard", element: Dashboard },
  { path: "/login", name: "Login", element: Login },
];

export default routes;
