import React from "react";
import { MdFileCopy, MdDashboard } from "react-icons/md";

const _nav = [
  {
    // component: CNavItem,
    name: "Dashboard",
    to: "/",
    icon: <MdDashboard />,
    badge: {
      color: "info",
    },
  },
  {
    // component: CNavTitle,
    name: "Inventory",
  },
  {
    // component: CNavItem,
    name: "Product",
    to: "/inventory/product",
    icon: <MdFileCopy />,
  },
];

export default _nav;
