import { AnimatePresence } from "framer-motion";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Layout from "./layout/Layout";
import Login from "./views/Login";
import { Suspense } from "react";
import PrivateOutlet from "./PrivateOutlet";
import Loading from "./components/Loading";
import { useSelector } from "react-redux";

function App() {
  const authed = useSelector((state) => state.authed);
  return (
    <AnimatePresence mode="wait">
      <Router>
        <Suspense fallback={Loading}>
          <Routes>
            <Route authed={authed} exact path="*" element={<PrivateOutlet />}>
              <Route authed={authed} path="*" element={<Layout />} />
            </Route>
            <Route path="/login" name="Login Page" element={<Login />} />
            {/* <Route exact path="/logout" element={<Navigate to="/login" />} /> */}
            {/* <Route exact path="/register" name="Register Page" element={<Register />} />
          <Route exact path="/404" name="Page 404" element={<Page404 />} />
          <Route exact path="/500" name="Page 500" element={<Page500 />} /> */}
          </Routes>
        </Suspense>
      </Router>
    </AnimatePresence>
  );
}

export default App;
